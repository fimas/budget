let budget = {
    income: [],
    expenditure: [],
    
    // Memeber function to add an income or expenditure item
    addItem: function (type, description, amount) {
        switch (type) {
            case 'inc':
                this.income.push({description: description, amount: amount});
                break;
            case 'exp':
                this.expenditure.push({description: description, amount: amount});
                break;
            default:
                console.error("Unknown category:", type);
        }
    },

    totalIncome: function() {
        const sum = this.income.reduce((total, item) => {
            const numericValue = Number(item.amount);

            if(!isNaN(numericValue)) {
                return total + numericValue;
            }

            return total;
        }, 0);

        return sum;
    },

    totalExpenditure: function() {
        const sum = this.expenditure.reduce((total, item) => {
            const numericValue = Number(item.amount);

            if(!isNaN(numericValue)) {
                return total + numericValue;
            }

            return total;
        }, 0);

        return sum;
    },
};

function formatNumberSwedishStyle(input) {
    // Ensure the input is treated as a floating point number
    let number = parseFloat(input);

    // Check if the number is a valid floating point number
    if (!isNaN(number)) {
        // Fix the number to two decimal places as a string
        let numberFixed = number.toFixed(2);

        // Split the string into whole and decimal parts
        let parts = numberFixed.split('.');

        // Replace the dot with a comma for the decimal part
        let decimalPart = parts[1];

        // Format the whole part with a non-breaking space as thousands separator
        let wholePart = parts[0]
            .split('') // Convert to array for reverse iteration
            .reverse() // Reverse to start grouping from the lowest order
            .join('') // Convert back to string
            .match(/.{1,3}/g) // Match groups of three characters
            .join('\u00A0') // Join groups with non-breaking space
            .split('') // Convert to array again
            .reverse() // Reverse to original order
            .join(''); // Convert back to string

        // Combine the whole part and decimal part
        return wholePart + ',' + decimalPart;
    } else {
        return 'Invalid number';
    }
}

document.addEventListener('DOMContentLoaded', (e) => {
    const incomeBtn = document.getElementById('incBtn');
    const expenseBtn = document.getElementById('expBtn');
    const incomeItemBox = document.getElementById('incItem');
    const incomeAmountBox = document.getElementById('incAmount');
    const expenseItemBox = document.getElementById('expItem');
    const expenseAmountBox = document.getElementById('expAmount');

    incomeBtn.addEventListener('click', (e) => {
        const type = 'inc';
        const item = incomeItemBox.value;
        const amount = incomeAmountBox.value;

        if(item && amount) {
            budget.addItem(type, item, amount);
            updateUI(type);

            incomeItemBox.value = '';
            incomeAmountBox.value = '';
        }
    });

    expenseBtn.addEventListener('click', (e) => {
        const type = 'exp';
        const item = expenseItemBox.value;
        const amount = expenseAmountBox.value;
        
        budget.addItem(type, item, amount);

        updateUI(type);

        expenseItemBox.value = '';
        expenseAmountBox.value = '';
    });
});

function addRow(table, item, amount) {
    let row = table.insertRow();
    let cellItem = row.insertCell(0);
    let cellAmount = row.insertCell(1);
    let cellActions = row.insertCell(2);

    let amountText = formatNumberSwedishStyle(amount);

    cellItem.textContent = item;
    cellAmount.textContent = amountText;
    cellActions.innerHTML = '<a href="#">Delete</a> <a href="#">Edit</a>';
}

function updateSum() {
    const totalContainer = document.getElementById('totalNum');

    let sum = budget.totalIncome() - budget.totalExpenditure();
    totalContainer.textContent = `${sum.toLocaleString()}`;
}

function updateUI(type) {
    const incTableBody = document.getElementById('incTableBody');
    const expTableBody = document.getElementById('expTableBody');
    

    switch(type) {
        case 'inc':
            incTableBody.innerHTML = '';

            for (income of budget.income) {
                addRow(incTableBody, income.description, income.amount);
                updateSum();
            }
            break;
        case 'exp':
            expTableBody.innerHTML = '';

            for (expenditure of budget.expenditure) {
                addRow(expTableBody, expenditure.description, expenditure.amount);
                updateSum();
            }
            break;
        default:
            console.error("Unknow type:", type);
    }
}
